# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-20 17:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myrm', '0005_auto_20170620_1509'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filelist',
            name='status',
            field=models.CharField(default='None', max_length=256),
        ),
        migrations.AlterField(
            model_name='operationlist',
            name='status',
            field=models.BooleanField(default=False),
        ),
    ]
