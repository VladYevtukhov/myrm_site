"""Bin module.

This module handles the execution of tasks.
"""
from work.process import deleteFile
from work.bin import clearFile, recoveryFile
from work.policies import autoErase
import errno
import re
import threading
from myrm.models import TrashList, OperationList, TaskList, DoneOperationList
import os
import time
import logging
import random


max_connections = 4
block = threading.BoundedSemaphore(value=max_connections)


def launch_check():
    """Launch_check function.

    Distribution of tasks for trash
    """
    operations = OperationList.objects.all()
    trash_list = TrashList.objects.all()
    status = False
    for trash in trash_list:
        if trash.active:
            status = True
            break
    if not status:
        if len(operations) != 0:
            new_thread = threading.Thread(target=execution)
            new_thread.start()
    if status and len(operations) == 0:
        for trash in trash_list:
            if trash.active:
                trash.active = False
                trash.save()


def execution():
    """Execution function.

    Distribution of tasks for trash.
    """
    while True:
        operations = OperationList.objects.all()
        task_list = TaskList.objects.all()
        if len(operations) == 0:
            break
        for task in task_list:
            if not task.trash.active and not task.process:
                new_thread = threading.Thread(target=thread_task, args=(task,))
                task.trash.active = True
                task.trash.save()
                task.process = True
                task.save()
                new_thread.setDaemon(True)
                new_thread.start()
        time.sleep(0.1)


def thread_task(task):
    """Thread_task function.

    Splitting the task into separate operations. Configuring Logging.

    Params:
        task: current task.
    """
    block.acquire()
    logger = logging.getLogger(task.trash.path_to_trash)
    logger.setLevel(logging.DEBUG)
    log = logging.FileHandler(filename=os.path.join(task.trash.path_to_trash, "logfile.log"))
    log.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s: %(name)s: %(message)s')
    log.setFormatter(formatter)
    logger.addHandler(log)
    threads = []
    index = 0
    operations = OperationList.objects.all()
    for operation in operations:
        if index > 2:
            while True:
                check = False
                for thread in threads[:]:
                    if not thread.isAlive():
                        threads.remove(thread)
                        index -= 1
                        check = True
                if check:
                    break
                time.sleep(0.2)
        if index <= 2:
            if operation.task == task:
                new_thread = threading.Thread(target=the_process, args=(operation,))
                new_thread.daemon = True
                threads.append(new_thread)
                new_thread.start()
                index += 1
    while index > 0:
        for thread in threads[:]:
            if not thread.isAlive():
                threads.remove(thread)
                index -= 1
        time.sleep(0.1)
    logger.removeHandler(log)
    task.trash.active = False
    task.trash.save()
    block.release()


def the_process(operation):
    """The_process function.

    Checking operations and their implementation.

    Params:
        operation: current operation.
    """
    trash_file_path = os.path.join(operation.trash, "files/")
    trash_info_path = os.path.join(operation.trash, "info/")
    error_message = None
    try:
        if operation.operation_name == "delete":
            deleteFile(operation.content, trash_file_path, trash_info_path, False, operation.task.trash.max_memory,
                       operation.task.trash.trash_file_max_size, operation.task.trash.enable_silent_mode,
                       operation.trash, operation.task.trash.enable_dry_run)
        elif operation.operation_name == "recovery":
            recoveryFile(operation.content, trash_file_path, trash_info_path, operation.task.trash.enable_force_overwrite,
                         operation.task.trash.enable_dry_run, operation.task.trash.enable_silent_mode)
        elif operation.operation_name == "clear":
            clearFile(operation.content, trash_file_path, trash_info_path, operation.task.trash.enable_dry_run,
                      operation.task.trash.enable_silent_mode)
        elif operation.operation_name == "clear_all":
            clearFile(operation.content, trash_file_path, trash_info_path, operation.task.trash.enable_dry_run,
                      operation.task.trash.enable_silent_mode)
    except OSError as e:
        if e.args[0] == errno.ENOENT:
            error_message = "File not found"
        elif e.args[0] == errno.EACCES:
            error_message = "Permission denied"
        elif e.args[0] == errno.EPERM:
            error_message = "Operation not allowed"
    finally:
        if error_message is None:
            dop = DoneOperationList(trash=operation.trash, operation_name=operation.operation_name,
                                    file_name=operation.content, status="Ok", task=operation.task)
        else:
            dop = DoneOperationList(trash=operation.trash, operation_name=operation.operation_name,
                                    file_name=operation.content, status=error_message, task=operation.task)
        dop.save()
        operation.delete()


def regular_delete(files, path, regular, current_trash, task):
    """Regular_delete function.

    Creating Regular Routines.

    Params:
        files: files to check for a regular expression.
        path: the path in which the test is being carried out.
        regular: regular expression.
        current_trash: path to trash.
        task: membership of the task.
    """
    for file in files:
        if re.search(regular, file):
            oper_list = OperationList(trash=current_trash.path_to_trash, operation_name="delete", content=os.path.join(path, file),
                                      task=task)
            oper_list.save()
        elif os.path.isdir(os.path.join(path, file)):
            file_from_file = os.listdir(os.path.join(path, file))
            regular_delete(file_from_file, os.path.join(path, file), regular, current_trash, task)


def auto_erase(current_trash):
    """Auto_erase function.

    Erase file from the trash if the time has elapsed that it is exceeding the allowable.

    Params:
        current_trash: selected trash.
    """
    current_file = os.path.join(current_trash.path_to_trash, "files/")
    current_info = os.path.join(current_trash.path_to_trash, "info/")
    erase_files = autoErase(current_trash.time_until_auto_erase, current_file, current_info,
                            current_trash.enable_dry_run, current_trash.enable_silent_mode, library=True)
    if len(erase_files) > 0:
        task_name = rand_index()
        task = new_task(task_name, current_trash, "clear")
        for file in erase_files:
            oper_list = OperationList(trash=current_trash.path_to_trash, operation_name="clear",
                                      content=file, task=task)
            oper_list.save()
        launch_check()


def bool_converter(data):
    if data is None:
        return False
    elif data == "on":
        return True


def rand_index():
    """Rand_index function.

    Task name generation.

    Return: task name.
    """
    task_list = TaskList.objects.all()
    index = random.randint(0, 1000000)
    name = "task#{}".format(index)
    while True:
        correct_name = True
        for task in task_list:
            if task.task_name == name:
                correct_name = False
                index = random.randint(0, 1000000)
                name = "task#{}".format(index)
                break
        if correct_name:
            break
    return name


def new_task(task_name, trash, task_operation):
    """New_task function.

    Check for exceeding the number of tasks and creating a new task

    Params:
        task_name.
        trash: path to trash.
        task_operation: operation name.

    Return: created task.
    """
    if TaskList.objects.count() >= 1000:
        last_task = TaskList.objects.latest("create_date")
        last_task.delete()
    task = TaskList(task_name=task_name, trash=trash, operation_name=task_operation)
    task.save()
    return task
