# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import TrashList, OperationList, TaskList, DoneOperationList
from django.contrib import admin

admin.site.register(TrashList)
admin.site.register(OperationList)
admin.site.register(TaskList)
admin.site.register(DoneOperationList)
