# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class TrashList(models.Model):
    path_to_trash = models.CharField(max_length=256)
    enable_silent_mode = models.BooleanField(default=False)
    enable_force_overwrite = models.BooleanField(default=False)
    trash_file_max_size = models.IntegerField(default=15)
    max_memory = models.IntegerField(default=5000000)
    time_until_auto_erase = models.IntegerField(default=30000000)
    enable_dry_run = models.BooleanField(default=False)
    active = models.BooleanField(default=False)
    select = models.BooleanField(default=False)


class TaskList(models.Model):
    task_name = models.CharField(max_length=256)
    trash = models.ForeignKey(TrashList, on_delete=models.CASCADE, default=None)
    operation_name = models.CharField(max_length=128, default=None)
    process = models.BooleanField(default=False)
    create_date = models.DateField(auto_now_add=True)


class OperationList(models.Model):
    trash = models.CharField(max_length=256, default=None)
    operation_name = models.CharField(max_length=128, default=None)
    content = models.CharField(max_length=256)
    task = models.ForeignKey(TaskList, on_delete=models.CASCADE, default=None)


class DoneOperationList(models.Model):
    trash = models.CharField(max_length=256, default=None)
    operation_name = models.CharField(max_length=128)
    file_name = models.CharField(max_length=512, default=None)
    status = models.CharField(max_length=256, default=None)
    task = models.ForeignKey(TaskList, on_delete=models.CASCADE, default=None)
