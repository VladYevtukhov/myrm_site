# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from myrm.models import TrashList, OperationList, DoneOperationList, TaskList
from .forms import NewTrashForms
from django.shortcuts import render
from work.bin import trashWatch
from work.service import makeTrash
import os
import shutil
import time
from myrm import bin


def file_from_trash(request):
    trash_list = TrashList.objects.all()
    current_trash = None
    for trash in trash_list:
        if trash.select:
            current_trash = trash
            break
    error_message = None
    files = None
    if request.method == "POST":
        clear_all = request.POST.get("ClearAll")
        clear_file = request.POST.get("delete")
        recovery = request.POST.get("recovery")
        task_name = bin.rand_index()
        if recovery is not None:
            task = bin.new_task(task_name, current_trash, "recovery")
            oper_list = OperationList(trash=current_trash.path_to_trash, operation_name="recovery",
                                      content=recovery, task=task)
            oper_list.save()
        elif clear_file is not None:
            task = bin.new_task(task_name, current_trash, "clear")
            oper_list = OperationList(trash=current_trash.path_to_trash, operation_name="clear",
                                      content=clear_file, task=task)
            oper_list.save()
        elif clear_all is not None and current_trash is not None:
            trash_file_path = os.path.join(current_trash.path_to_trash, "files/")
            all_trash_file = os.listdir(trash_file_path)
            if len(all_trash_file) > 0:
                task = bin.new_task(task_name, current_trash, "clear_all")
                for filename in all_trash_file:
                    oper_list = OperationList(trash=current_trash.path_to_trash, operation_name="clear_all",
                                            content=filename, task=task)
                    oper_list.save()
        bin.launch_check()
    time.sleep(0.05)
    current_view = None
    recovery_files = []
    clear_files = []
    if current_trash is not None:
        current_view = os.path.join(current_trash.path_to_trash, "files/")
        if not os.path.exists(current_trash.path_to_trash):
            makeTrash(current_trash.path_to_trash)
    if current_view is not None:
        files = trashWatch(current_view, float("inf"))
        operations = OperationList.objects.all()
        for operation in operations:
            if operation.content in files:
                if operation.operation_name == "recovery":
                    recovery_files.append(operation.content)
                elif operation.operation_name == "clear" or operation.operation_name == "clear_all":
                    clear_files.append(operation.content)
    else:
        error_message = "Trash not selected"
    context = {
        "error_message": error_message,
        "files": files,
        "recovery_files": recovery_files,
        "clear_files": clear_files
    }
    return render(request, "myrm/files_from_trash.html", context)


def trash_records(request):
    trash_list = TrashList.objects.all()
    error_message = None
    if request.method == "POST":
        validation = request.POST.get("form")
        if validation is not None:
            form = NewTrashForms(request.POST or None)
            if form.is_valid():
                data = form.cleaned_data
                check = False
                for trash in trash_list:
                    if trash.path_to_trash == data["path_to_trash"]:
                        check = True
                if not check and data["path_to_trash"] != "":
                    ts = TrashList(path_to_trash=data["path_to_trash"], enable_silent_mode=data["enable_silent_mode"],
                                   enable_force_overwrite=data["enable_force_overwrite"],
                                   trash_file_max_size=data["trash_file_max_size"], max_memory=data["max_memory"],
                                   time_until_auto_erase=data["time_until_auto_erase"],
                                   enable_dry_run=data["enable_dry_run"])
                    ts.save()
                    try:
                        makeTrash(data["path_to_trash"])
                    except OSError as e:
                        ts.delete()
                        error_message = e
        else:
            form = NewTrashForms(None)
        selected_trash = request.POST.get("confirm_trash")
        delete_selected_record = request.POST.get("del_trash")
        edit_record = request.POST.get("edit_trash")
        if delete_selected_record is not None:
            for trash in trash_list:
                if trash.path_to_trash == delete_selected_record:
                    trash.delete()
                    if os.path.exists(delete_selected_record):
                        shutil.rmtree(delete_selected_record)
        elif edit_record is not None:
            sm = request.POST.get("enable_silent_mode {}".format(edit_record))
            fo = request.POST.get("enable_force_overwrite {}".format(edit_record))
            tms = request.POST.get("trash_file_max_size {}".format(edit_record))
            mm = request.POST.get("max_memory {}".format(edit_record))
            ae = request.POST.get("time_until_auto_erase {}".format(edit_record))
            dr = request.POST.get("enable_dry_run {}".format(edit_record))
            for trash in trash_list:
                if trash.path_to_trash == edit_record:
                    trash.enable_silent_mode = bin.bool_converter(sm)
                    trash.enable_force_overwrite = bin.bool_converter(fo)
                    if str(tms).isdigit():
                        trash.trash_file_max_size = tms
                    if str(mm).isdigit():
                        trash.max_memory = mm
                    if str(ae).isdigit():
                        trash.time_until_auto_erase = ae
                    trash.enable_dry_run = bin.bool_converter(dr)
                    trash.save()
        elif selected_trash is not None:
            for trash in trash_list:
                if trash.select:
                    trash.select = False
                    trash.save()
                if trash.path_to_trash == selected_trash:
                    trash.select = True
                    trash.save()
    else:
        form = NewTrashForms(None)
    list_of_trash = TrashList.objects.all()
    context = {
        "error_message": error_message,
        "form": form,
        "list_of_trash": list_of_trash,
    }
    return render(request, "myrm/trash_list.html", context)


def transfer(request):
    error_message = None
    files_for_delete = set()
    trash_list = TrashList.objects.all()
    current_trash = None
    if request.method == "POST":
        new_path = request.POST.get("directory")
        path = request.POST.get("delete")
        regular_path = request.POST.get("regular_delete")
        if path is not None:
            current_dir_file = os.listdir(path)
        elif regular_path is not None:
            current_dir_file = os.listdir(regular_path)
        else:
            current_dir_file = os.listdir(new_path)
        if path is not None or regular_path is not None:
            for files in current_dir_file:
                if path is not None:
                    checkbox = request.POST.get("check {}".format(os.path.join(path, files)))
                    if checkbox is not None:
                        files_for_delete.add(os.path.join(path, files))
                else:
                    checkbox = request.POST.get("check {}".format(os.path.join(regular_path, files)))
                    if checkbox is not None:
                        files_for_delete.add(files)
            for trash in trash_list:
                if trash.select:
                    current_trash = trash
                    break
            if current_trash is not None:
                bin.auto_erase(current_trash)
                task_name = bin.rand_index()
                if not os.path.exists(current_trash.path_to_trash):
                    makeTrash(current_trash.path_to_trash)
                if path is not None:
                    if len(files_for_delete) > 0:
                        task = bin.new_task(task_name, current_trash, "delete")
                        for files in files_for_delete:
                                oper_list = OperationList(trash=current_trash.path_to_trash, operation_name="delete",
                                                          content=os.path.join(path, files), task=task)
                                oper_list.save()
                    bin.launch_check()
                elif regular_path is not None:
                    path = regular_path
                    regular = request.POST.get("regular")
                    if len(files_for_delete) > 0:
                        task = bin.new_task(task_name, current_trash, "delete")
                        bin.regular_delete(files_for_delete, regular_path, regular, current_trash, task)
                    bin.launch_check()
            else:
                if regular_path is not None:
                    path = regular_path
                error_message = "Trash not selected"
        elif new_path is not None:
            path = new_path
        current_dir = set()
        current_file = set()
        for current in current_dir_file:
            if os.path.isdir(os.path.join(path, current)):
                current_dir.add(os.path.join(path, current))
            else:
                current_file.add(os.path.join(path, current))
    else:
        for trash in trash_list:
            if trash.select:
                current_trash = trash
                break
        if current_trash is None:
            error_message = "Trash not selected"
        path = os.path.expanduser('~')
        current_dir_file = os.listdir(path)
        current_dir = set()
        current_file = set()
        for current in current_dir_file:
            if os.path.isdir(os.path.join(path, current)):
                current_dir.add(os.path.join(path, current))
            else:
                current_file.add(os.path.join(path, current))
    context = {
        "error_message": error_message,
        "path": path,
        "current_dir": current_dir,
        "current_file": current_file
    }
    return render(request, "myrm/transfer.html", context)


def opers(request):
    operations = OperationList.objects.all()
    context = {
        "operations": operations
    }
    return render(request, "myrm/operations.html", context)


def last_records(request):
    if request.method == "POST":
        erase = request.POST.get("erase")
        if erase is not None:
            DoneOperationList.objects.all().delete()
            TaskList.objects.all().delete()
    files = DoneOperationList.objects.all()
    context = {
        "files": files
    }
    return render(request, "myrm/last_records.html", context)


def tasks(request):
    task = True
    done_operation_list = DoneOperationList.objects.all()
    operation_list = OperationList.objects.all()
    current_done_files = set()
    current_files = set()
    if request.method == "POST":
        current_task = request.POST.get("task")
        erase = request.POST.get("erase")
        if erase is not None:
            TaskList.objects.all().delete()
        elif current_task is not None:
            task = False
            for operation in done_operation_list:
                if operation.task.task_name == current_task:
                    current_done_files.add(operation)
            for operation in operation_list:
                if operation.task.task_name == current_task:
                    current_files.add(operation)
        else:
            task = True
    task_list = TaskList.objects.all()
    context = {
        "task_list": task_list,
        "task": task,
        "current_done_files": current_done_files,
        "current_files": current_files
    }
    return render(request, "myrm/tasks.html", context)
