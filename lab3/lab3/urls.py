"""lab3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from myrm import views
from django.contrib import admin

urlpatterns = [
    url(r'^$', views.trash_records, name="trash_records"),
    url(r'^transfer/', views.transfer, name="transfer"),
    url(r'^last_records/', views.last_records, name="last_records"),
    url(r'^tasks/', views.tasks, name="tasks"),
    url(r'^operations/', views.opers, name="operations"),
    url(r'^files_from_trash/', views.file_from_trash, name="file_from_trash"),
    url(r'^admin/', admin.site.urls),
]
