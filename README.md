# MYRM #

Myrm manager is designed to work for the user with different trash cans, each of which has its own log file.

### With this manager you can ###

* View and change the settings of each trash
* View local directories
* Delete selected files and directories
* Delete selected files and directories by regular expression
* Recover deleted files
* Erase deleted files
* View running operations
* View the history of tasks and operations

### Prerequisites ###

* Pip
* Python
* Django

### How do I get set up? ###

* Clone or download https://bitbucket.org/VladYevtukhov/myrm and setup
* Сlone or download this repository
* Run server via python django
	* python manage.py runserver
* Start the browser and enter "http://127.0.0.1:8000/" in the address bar

### Who do I talk to? ###

* Vlad Yevtukhov
* nyti96@yandex.ru